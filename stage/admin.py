from django.contrib import admin
from stage.models import Diplome,Promotion,Personne,Etudiant,Enseignant,Stage,logiciel,Stage_logiciel,EnseignantResp


admin.site.register(Diplome)
admin.site.register(Promotion)
admin.site.register(Personne)
admin.site.register(Etudiant)
admin.site.register(Enseignant)
admin.site.register(Stage)
admin.site.register(logiciel)
admin.site.register(Stage_logiciel)
admin.site.register(EnseignantResp)
